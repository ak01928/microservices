# Instructions

## Step 1: microservices introductory demo

Often, we want to contribute to or further develop an open-source project in isolation from the original. We achieve this by forking a project for our personal use (always check license), which has some benefits over branching. Click Fork at the top right corner of this project's home page, and then you have a personal copy of the repository which you can change to your heart's content. Clone the repository to your Azure VM (or you can also use your own laptop if you have python3 and Docker installed ** It will not work on Windows unless you have Windows server** It will likely work with MacOS and Linux.).

You will see that you are on the  `master` branch, with two files `movies.py` and `showtimes.py`. Look through their source code to see how they function. `movies.py` fetches movie data from a json file in `/database`. `showtimes.py` fetches showtimes for the movies from another json file in `/database`. Each of them is a **microservice** which exposes RESTful API endpoints. You can run them as follows:
```console
python3 movies.py
```
Now, the movies microservice is running. You can query it by sending HTTP GET requests. Note that we are using the port 5001 on localhost (127.0.0.1) as that is how the server is configured (see towards the bottom of the file in movies.py)
```console
curl http://127.0.0.1:5001/ #returns list of endpoints exposed
curl http://127.0.0.1:5001/movies #returns list of all movies
curl http://127.0.0.1:5001/movies/720d006c-3a57-4b6a-b18f-9b713b073f3c #returns details of one movie
```

Similarly, you can run the showtimes microservice
```console
python3 showtimes.py
```
You can query its endpoints too:
```console
$ curl 127.0.0.1:5002/      
{"uri": "/", "subresource_uris": {"showtimes": "/showtimes", "showtime": "/showtimes/<date>"}}%                                                                 
$ curl 127.0.0.1:5002/showtimes
{"20151130": ["720d006c-3a57-4b6a-b18f-9b713b073f3c", "a8034f44-aee4-44cf-b32c-74cf452aaaae", "39ab85e5-5e8e-4dc5-afea-65dc368bd7ab"], "20151201": ["267eedb8-0f5d-42d5-8f43-72426b9fb3e6", "7daf7208-be4d-4944-a3ae-c1c2f516f3e6", "39ab85e5-5e8e-4dc5-afea-65dc368bd7ab", "a8034f44-aee4-44cf-b32c-74cf452aaaae"], "20151202": ["a8034f44-aee4-44cf-b32c-74cf452aaaae", "96798c08-d19b-4986-a05d-7da856efb697", "39ab85e5-5e8e-4dc5-afea-65d
```

## Step 2: Making microservices talk to each other

As with the movies, the showtimes microservice allows you to query one single record. For a date that it knows about (e.g., 30 Nov 2015 or `20151130`), you can ask what movies were shown on that date.
```console
$ curl 127.0.0.1:5002/showtimes/20151130 
[
    "720d006c-3a57-4b6a-b18f-9b713b073f3c",
    "a8034f44-aee4-44cf-b32c-74cf452aaaae",
    "39ab85e5-5e8e-4dc5-afea-65dc368bd7ab"
]%  
```
You get back a list of movie *IDs*. This is nice, but not wholly satisfactory. Your next task is to modify the `showtimes_record` function to contact the movies microservice to get the movie title given its ID, and make the showtimes service return back a more user-friendly response that looks like this:
```console
$ curl 127.0.0.1:5002/showtimes/20151130
[
    "The Good Dinosaur",
    "The Martian",
    "Spectre"
]%               
```
>Try your best to solve it. If you need hints, you will find the solution for this in the branch `simpleservices`, which you can obtain by running `git checkout simpleservices`. Note that after checking out this branch, you will have to start both the `movies` and `showtimes` microservices and then issue the curl request. The showtimes microservice will talk to movies microservice to deliver the final result.

> This tutorial is continued now in the `simpleservices` branch

## Step 3: Dockerization of microservices

## Step 4: How to make two dockerized microservices talk to each other

# credits 
The original code is taken from https://github.com/umermansoor/microservices
It has been lightly modified for Python3 compatibility, and further simplified to showcase microservice communications.
We have also added a demo of dockerization
